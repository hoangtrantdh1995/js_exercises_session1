/**
 *Input:
 *       real number R:
 *       R1 = 5;
 *       R2 = 2.5;
 *       R3 = 7;
 *       R4 = 200;
 *       R5 = 1234.5;
 *
 *Steps to take:
 *
 *Output:
 *
 *       The average value R
 *
 */

var R1 = 5;
var R2 = 2.5;
var R3 = 7;
var R4 = 200;
var R5 = 1234.5;

// The average value R
var the_average_value_R = R1 + R2 + R3 + R4 + R5;
console.log("the_averrage_value_R: ", the_average_value_R);
