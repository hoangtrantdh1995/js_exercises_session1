/*Input:
 *      positive integer n with 2 digits: n = 29;
 *
 *Steps to take:
 *
 *Output:
 *      total 2 digits = 11;
 *
 *
 */

var n = 29;

// units
var units = Math.floor(n % 10);
console.log("units: ", units);
// dozens
var dozens = Math.floor(n / 10);
console.log("dozens: ", dozens);

var total_2_digits = units + dozens;
console.log("total_2_digits: ", total_2_digits);
