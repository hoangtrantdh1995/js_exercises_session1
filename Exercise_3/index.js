/*Input:
 *      currency: 1usd = 23.500vnd
 *      user input = 5usd
 *
 *Steps to take:
 *
 *Output:
 *      user conversion results
 *
 *
 */
var currency_usd = 5;
var domestic_money_vnd = 23.5;

// user conversion results
var conversion_result = currency_usd * domestic_money_vnd;
console.log("conversion_result: ", conversion_result);
