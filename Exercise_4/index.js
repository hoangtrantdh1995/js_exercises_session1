/*
 *Input:
 *     length of rectangle = 30
 *     rectangle width = 15
 *
 *Steps to take:
 *
 *Output:
 *
 *     perimeter of a rectangle
 *     rectangular area
 */

var length_rectangle = 30;
var width_rectangle = 15;

// perimeter of a rectangle
var perimeter_rectangle = (length_rectangle + width_rectangle) * 2;
console.log("perimeter_rectangle: ", perimeter_rectangle);

//rectangular area
var rectangle_area = length_rectangle * width_rectangle;
console.log("rectangle_area: ", rectangle_area);
