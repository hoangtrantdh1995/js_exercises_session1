/*Input:
 *      one day salary = 100.000;
 *      number of workdays = 26;
 *
 *Steps to take:
 *
 *Output:
 *
 *      total wages
 *
 */

var one_day_salary = 100.000;
var number_of_workdays = 26;

// total wages
var total_wages = one_day_salary * number_of_workdays;
console.log("total_wages: ", total_wages);
